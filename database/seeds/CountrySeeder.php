<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('countries')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $users =
            array(
                array('country_code' => 'USA', 
                        'name'=>'United State', 
                        'icon'=>'america_icon.png', 
                        'is_default'=>'1', 
                        'status'=>'1', 
                        'dialing_code'=> '+1'),
                array('country_code' => 'IN', 
                        'name'=>'India', 
                        'icon'=>'ind_icon.png', 
                        'is_default'=>'0', 
                        'status'=>'1', 
                        'dialing_code'=> '+91'),
            );

        DB::table('countries')->insert($users);
    }
}
