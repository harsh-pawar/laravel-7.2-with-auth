<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $users =
            array(
                array('email' => 'dev@yinka.com', 
                        'password'=>Hash::make("abc123"), 
                        'name'=> 'Dev',
                        'login_with'=>'E',
                        'is_premium'=>1,
                        'phone_number'=>'9033327297',
                        'photo'=>'',
                        'city'=>'Ahmedabad',
                        'dob'=>'1991/09/05',
                        'country_id'=>1,
                        'account_type'=>1,
                    ),
            );

        DB::table('users')->insert($users);
    }
}
