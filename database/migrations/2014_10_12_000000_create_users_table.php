<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('facebook_id')->default('');
            $table->string('google_id')->default('');
            $table->string('apple_id')->default('');
            $date = date('Y/m/d');
            $table->date('dob')->default($date);
            $table->set('gender', ['M', 'F', 'O'])->comment('M-Male, F-Female, O-Other')->default('M');
            $table->set('login_with', ['E', 'A', 'G', 'F'])->comment('E-Email, A-Apple, G-Google, F-Facebook')->default('E');
            $table->boolean('is_premium')->default(0);
            $table->string('phone_number',20)->default('');
            $table->string('photo')->default('');
            $table->string('city')->default('');
            //country_id column add after country table create.
            $table->tinyInteger('account_type')->comment('1-Admin, 2-Normal, 3-Guest')->default(2);
            $table->string('token')->default('');
            $table->string('code')->comment('For OTP or varification purpose')->default('');
            $table->set('status', ['0', '1'])->comment('0-InActive, 1-Active')->default(1);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
