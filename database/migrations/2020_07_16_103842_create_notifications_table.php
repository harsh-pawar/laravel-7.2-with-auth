<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->set('new_competition_alert', ['0', '1'])->comment('0-Off, 1-On')->default(0);
            $table->set('leader_board_movement_down', ['0', '1'])->comment('0-Off, 1-On')->default(0);
            $table->set('leader_board_movement_up', ['0', '1'])->comment('0-Off, 1-On')->default(1);
            $table->set('competition_ending', ['0', '1'])->comment('0-Off, 1-On')->default(1);
            $table->set('voting_follow_alert', ['0', '1'])->comment('0-Off, 1-On')->default(1);
            $table->set('event_notification', ['0', '1'])->comment('0-Off, 1-On')->default(1);

            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();

            $table->timestamps();
        });

        Schema::table('notifications', function($table) {
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
