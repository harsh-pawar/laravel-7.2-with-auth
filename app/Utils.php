<?php

namespace App;


class Utils {


    public static function checkHeader($request) {

        $authoriseze = $request->headers->has('Authorization');
        if(!$authoriseze){
            return "Authorization";
        }
        $content_type = $request->headers->has('Content-Type');
        if(!$content_type){
            return "Content-Type";
        }
        $accepts = $request->headers->has('Accept');
        if(!$accepts){
            return "Accept";
        }
        $lancode = $request->headers->has('lancode');
        if(!$lancode){
            return "lancode";
        }
        $platform = $request->headers->has('platform');
        if(!$platform){
            return "platform";
        }
        $appversion = $request->headers->has('appversion');
        if(!$appversion){
            return "appversion";
        }
        $timezone = $request->headers->has('timezone');
        if(!$timezone){
            return "timezone";
        }
        
        return "";

    }

}