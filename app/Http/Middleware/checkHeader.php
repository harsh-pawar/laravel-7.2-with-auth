<?php

namespace App\Http\Middleware;

use Closure;
use Response;

class checkHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authoriseze = $request->headers->has('Authorization');
        if(!$authoriseze){  
            return Response::json(array('error'=>'Please set Authorization header'));  
        }
        $content_type = $request->headers->has('Content-Type');
        if(!$content_type){
            return Response::json(array('error'=>'Please set Content-Type header'));  
        }
        $accepts = $request->headers->has('Accept');
        if(!$accepts){
            return Response::json(array('error'=>'Please set Accept header')); 
        }
        $lancode = $request->headers->has('lancode');
        if(!$lancode){
            return Response::json(array('error'=>'Please set lancode header')); 
        }
        $platform = $request->headers->has('platform');
        if(!$platform){
            return Response::json(array('error'=>'Please set platform header')); 
        }
        $appversion = $request->headers->has('appversion');
        if(!$appversion){
            return Response::json(array('error'=>'Please set appversion header')); 
        }
        $timezone = $request->headers->has('timezone');
        if(!$timezone){
            return Response::json(array('error'=>'Please set timezone header')); 
        }  

        return $next($request);
    }
}
