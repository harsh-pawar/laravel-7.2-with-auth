<?php 

namespace App\Http\Controllers\API;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User;
use App\Utils;
use Illuminate\Support\Facades\Auth; 
use Validator;

class UserController extends Controller {

    public $successStatus = 200;
    
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(Request $request){ 

        $input = $request->all();
        if(isset($input['login_with']) && $input['login_with'] == 'E'){
            $validator = Validator::make($request->all(), [ 
                'email' => 'required', 
                'password' => 'required'
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
                $user = Auth::user(); 
                $success['token'] =  $user->createToken('MyApp')->accessToken; 
                $success['user'] = $user;
                $success['message'] ="Login successfully.";
                return response()->json(['success' => $success], $this->successStatus); 
            } else { 
                return response()->json(['error'=>'Unauthorised'], 401); 
            } 
        } else if(isset($input['login_with']) && $input['login_with'] == 'F') {
            $validator = Validator::make($request->all(), [ 
                'login_id' => 'required', 
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }
            $user = User::where('login_id',$input['login_id'])->first();
            if(isset($user) && !empty($user)) {
                $success['token'] =  $user->createToken('MyApp')->accessToken; 
                $success['user'] = $user;
                $success['message'] ="Login successfully.";
                return response()->json(['success' => $success], $this->successStatus);
            } else {
                $response['is_new_user'] = 1;
                $response['message'] = "No Facebook User found.";
                return response()->json([$response,'error'=>'Please enter proper login_with parameter.'], 401);
            }
        } else if(isset($input['login_with']) && $input['login_with'] == 'G') {
            $validator = Validator::make($request->all(), [ 
                'login_id' => 'required', 
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }
            $user = User::where('login_id',$input['login_id'])->first();
            if(isset($user) && !empty($user)) {
                $success['token'] =  $user->createToken('MyApp')->accessToken; 
                $success['user'] = $user;
                $success['message'] ="Login successfully.";
                return response()->json(['success' => $success], $this->successStatus);
            } else {
                $response['is_new_user'] = 1;
                $response['message'] = "No Facebook User found.";
                return response()->json([$response,'error'=>'Please enter proper login_with parameter.'], 401);
            }
        } else if(isset($input['login_with']) && $input['login_with'] == 'A') {
            $validator = Validator::make($request->all(), [ 
                'login_id' => 'required', 
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }
            $user = User::where('login_id',$input['login_id'])->first();
            if(isset($user) && !empty($user)) {
                $success['token'] =  $user->createToken('MyApp')->accessToken; 
                $success['user'] = $user;
                $success['message'] ="Login successfully.";
                return response()->json(['success' => $success], $this->successStatus);
            } else {
                $response['is_new_user'] = 1;
                $response['message'] = "No Facebook User found.";
                return response()->json([$response,'error'=>'Please enter proper login_with parameter.'], 401);
            }
        } else {
            return response()->json(['error'=>'Please enter proper login_with parameter.'], 401); 
        }
    }
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'gender' => 'required', 
            'email' => 'required|email|unique:users', 
            'city' => 'required', 
            'country_id' => 'required', 
            'phone_number' => 'required|unique:users', 
            // 'password' => 'required', 
            // 'c_password' => 'required|same:password', 
        ]);
        
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        $input = $request->all(); 
        if(isset($input['password'])) {
            $input['password'] = bcrypt($input['password']); 
        } else {
            $input['password'] = bcrypt('abc123'); 
        }
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyApp')->accessToken; 
        $success['name'] =  $user->name;

        return response()->json(['success'=>$success], $this->successStatus); 
    }
/** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details(Request $request) 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 

    public function changePassword(Request $request) {
        $validator = Validator::make($request->all(), [ 
            'password' => 'required',
            'confirm_password' => 'required|same:password', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $user = Auth::user(); 
        $input = $request->all(); 
        $user->password = bcrypt($input['password']);
        $user->save();

        $success['message'] = "Password updated successfully.";

        return response()->json(['success'=>$success], $this->successStatus); 
    }
}