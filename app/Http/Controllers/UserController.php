<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    
    public function index() {

        $users = User::all();
        return view('users.index',compact('users'));

    }

    public function edit($id) {

        $user = User::find($id);

        return view('users.edit',compact('user'));

    }

    public function destroy($id){

        print_r($id);exit;

    }

}
