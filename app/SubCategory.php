<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
    //
    use SoftDeletes;
    protected $table = "album_sub_category";
    protected $softDelete = true;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'album_category_id', 'title', 'subtitle', 'sub_priority'
    ];

    public function category() {

        return $this->hasOne('App\Category','id','album_category_id');

    }

    public function images() {

        return$this->hasMany("App\ImageMapper",'sub_category_id');

    }
}
