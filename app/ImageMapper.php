<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImageMapper extends Model
{

    use SoftDeletes;
    protected $table = "image_mapper";
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'image_name', 'category_id', 'sub_category_id',
    ];

    public function category() {

        return $this->hasOne('App\Category','id','category_id');

    }

    public function subcategory() {

        return $this->hasOne('App\SubCategory','id','sub_category_id');

    }
}
