<div class="card-body">
    <div class="form-group row">
        {!! Form::label('email', 'E-Mail Address',['class'=>'col-sm-2 col-form-label']) !!}
      <div class="col-sm-10">
          {!! Form::email('email', $user->email,['placeholder'=>'Email', 'class'=>'form-control']) !!}
      </div>
    </div>
    <div class="form-group row">
        {!! Form::label('name', 'Name',['class'=>'col-sm-2 col-form-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('name', $user->name,['placeholder'=>'Name', 'class'=>'form-control']) !!}
      </div>
    </div>
    <div class="form-group row">
        {!! Form::label('phone', 'Phone No',['class'=>'col-sm-2 col-form-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('phone_number', $user->phone_number,['placeholder'=>'Phone No', 'class'=>'form-control']) !!}
      </div>
    </div>
    <div class="form-group row">
        {!! Form::label('dob', 'Date Of Birth',['class'=>'col-sm-2 col-form-label']) !!}
      <div class="col-sm-10">
        {!! Form::date('dob', $user->dob,['placeholder'=>'Date of Births', 'class'=>'form-control']) !!}
      </div>
    </div>
    <div class="form-group row">
        {!! Form::label('photo', 'Photo',['class'=>'col-sm-2 col-form-label']) !!}
      <div class="col-sm-10">
          <div class="position-relative">
                <img src="/images/default_profile_img.jpg" style='height:100px' alt="Profile Pic" class="img-fluid">
                {{-- <img src="{{$user->phone_number}}" alt="Profile Pic" class="img-fluid"> --}}
          </div>
      </div>
    </div>

    <div class="form-group row">
        {!! Form::label('city', 'City',['class'=>'col-sm-2 col-form-label']) !!}
      <div class="col-sm-10">
        {!! Form::text('city', $user->city,['placeholder'=>'City', 'class'=>'form-control']) !!}
      </div>
    </div>

    <div class="form-group row">
      {!! Form::label('gender', 'Gender',['class'=>'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
      {!! Form::select('gender', ['M'=>'Male','F'=>'Female'],$user->gender,['class'=>'form-control']) !!}
    </div>
  </div>

  <div class="form-group row">
      <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
        <input type="checkbox" class="custom-control-input" id="customSwitch3">
      </div>
  </div>

    
  </div>

<!-- /.card-body -->
<div class="card-footer">
    <button type="submit" class="float-sm-right btn btn-primary">Update</button>
</div>
