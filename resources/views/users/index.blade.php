@extends('layouts.admin')

@section('head-css')
    <link rel="stylesheet" href="/bower_components/admin-lte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/bower_components/admin-lte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
@endsection

@section('page-detail')
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0 text-dark">Users List</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/home">Home</a></li>
            <li class="breadcrumb-item active">Users</li>
        </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Users List</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Login with</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>                        
                        <td>{{ $user->email }}</td>                        
                        <td>{{ $user->login_with }}</td>                        
                        <td>{{ $user->status ? "Active" : "Inactive" }}</td>                        
                        <td>
                            <a title="Edit"  class="btn btn-primary" href="{{ url('/user/'.$user->id.'/edit') }}" style="margin:5px;">
                                <i class="ion ion-edit"></i>
                            </a>
                            @if($user->account_type != 1)
                                <form class="delete_user" action="{{ url('/user', ['id' => $user->id]) }}" method="post" style="display: inline-block;">
                                    <button title="Delete" class="btn btn-danger" type="submit"><i class="ion ion-close"></i></button>
                                    @method('delete')
                                    @csrf
                                </form>
                            @endif
                        </td>                        
                    </tr>    
                    @endforeach    
                </tbody>
                <tfoot>
                
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    
    <!-- /.card -->

@endsection

@section('js-script')
    <script src="/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/bower_components/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="/bower_components/admin-lte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/bower_components/admin-lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
            $(".delete_user").on("submit", function(){
                var r = confirm("Are you sure you want to add this User? It will not be recover.");
                if (r != true) {
                    return false;
                }
                return true;
            })
        });
    </script>

@endsection
